# Create the environment cdn S3 bucket
resource "aws_s3_bucket" "cloudfront" {
  bucket = "${var.cloudfront_name}-bucket"

  tags = {
    Name = "${var.cloudfront_name}-bucket"
  }
}

# Server Side Encryption
resource "aws_s3_bucket_server_side_encryption_configuration" "cloudfront" {
  bucket = aws_s3_bucket.cloudfront.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "AES256"
    }
  }
}

# Block public access to S3 Bucket
resource "aws_s3_bucket_acl" "cloudfront" {
  bucket = aws_s3_bucket.cloudfront.id
  acl    = "private"
}

# Make sure builds bucket cannot be public
resource "aws_s3_bucket_public_access_block" "cloudfront" {
  bucket                  = aws_s3_bucket.cloudfront.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# S3 bucket policy
data "aws_iam_policy_document" "cloudfront" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.cloudfront.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.cloudfront.iam_arn]
    }
  }
}

# Set S3 bucket policy
resource "aws_s3_bucket_policy" "cloudfront" {
  bucket = aws_s3_bucket.cloudfront.id
  policy = data.aws_iam_policy_document.cloudfront.json
}

# Upload test HTML page
resource "aws_s3_object" "object" {
  bucket       = aws_s3_bucket.cloudfront.id
  key          = "health.html"
  content      = file("${path.module}/health/health.html")
  content_type = "text/html"
}
