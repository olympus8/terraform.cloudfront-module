# Terraform.Cloudfront Module

A module to create a Cloudfront distribution in us-east-1 with originating bucket in defined region.

## Getting started


| Provider |        |
|----------|--------|
| AWS      | 4.15.1 |


## Dependencies

A public hosted zone is required to create an ACM

[Creating a public hosted zone](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html)

## main.tf

to use multiple providers we must set aliases within the provider block

```go
# Provider for Cloudfront ACM
provider "aws" {
  region = "us-east-1"
  alias  = "us-east-1"
  default_tags {
    tags = {
      Terraform   = "terraform.cloudfront"
    }
  }
}

```

## Using the Module

```go
module "cloudfront_test" {
  source                 = "../terraform.cloudfront-module"
  providers = {
    aws           = aws
    aws.us-east-1 = aws.us-east-1
  }
  dns_zone_id             = "Z0439023550222X2345"
  cloudfront_name         = "web"
  root_domain             = "sheep-sheerer.com"
}
```

## Region

The Cloudfront distribution must be created in `eu-west-1` as such the ACM certificate must be created in the same region. However, the orgin s3 bucket will be created in the region of the CLI users current context. 

# Default Cache Behaviour 

The default cache behaviour can be amended in the default_cache_behavior blocks.

```go
  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
...

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
```

## Health
A file is deployed to the /health.html endpoint to provide validation once the Cloudfront Distribution has been deployed

## Destroying 

This module does not use 'force_destroy'  This is forces the user to delete the content of the bucket prior to the module being destroyed.

> CloudFront distributions take about 15 minutes to reach a deployed state after creation or modification. During this time, deletes to resources will be blocked. If you need to delete a distribution that is enabled and you do not want to wait, you need to use the retain_on_delete flag.