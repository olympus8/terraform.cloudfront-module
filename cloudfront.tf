# Create environment Cloudwatch distribution
resource "aws_cloudfront_distribution" "cloudfront" {
  provider = aws.us-east-1
  origin {
    domain_name = aws_s3_bucket.cloudfront.bucket_domain_name
    origin_id   = "${var.cloudfront_name}-cdn"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cloudfront.cloudfront_access_identity_path
    }
  }

  aliases = ["${var.cloudfront_name}.cdn.${var.root_domain}"]

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.cloudfront_name}-cdn"

  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = "${var.cloudfront_name}-cdn"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  price_class = "PriceClass_100"

  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["GB"]
    }
  }

  viewer_certificate {
    acm_certificate_arn      = aws_acm_certificate.cloudfront.arn
    ssl_support_method       = "sni-only"
    minimum_protocol_version = "TLSv1.1_2016"
  }

  custom_error_response {
    error_caching_min_ttl = 10
    error_code            = 404
    response_code         = 200
    response_page_path = "/index.html"
  }

  custom_error_response {
    error_caching_min_ttl = 10
    error_code            = 403
    response_code         = 200
    response_page_path = "/index.html"
  }
}

# Create environment cdn Cloudfront OAI
resource "aws_cloudfront_origin_access_identity" "cloudfront" {
  comment = "${var.cloudfront_name}-cdn"
}
