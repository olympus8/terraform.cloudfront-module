# Cloudfront ACM certificate for subdomain
resource "aws_acm_certificate" "cloudfront" {
  provider          = aws.us-east-1
  domain_name       = "${var.cloudfront_name}.cdn.${var.root_domain}"
  validation_method = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

# ACM certificate validation via Route53 for subdomain
resource "aws_acm_certificate_validation" "cloudfront" {
  provider                = aws.us-east-1
  certificate_arn         = aws_acm_certificate.cloudfront.arn
  validation_record_fqdns = [for record in aws_route53_record.cloudfront_validation : record.fqdn]
  depends_on              = [aws_route53_record.cloudfront_validation]
}
